import java.io.File;
import java.io.PrintWriter;
import java.util.EmptyStackException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TestCycle {

	static double wheels, weight;
	static File filename = new File("F:/Workspace/Unit7Output/src/Output.txt");
	
	public static void main(String[] args) throws Exception {
		
		PrintWriter output = new PrintWriter(filename);
		Scanner input = new Scanner(System.in);
		
		try {
			wheels = input.nextDouble();
			weight = input.nextDouble();
			input.close();
			if (wheels <= 0 || weight <=0) {
				throw new EmptyStackException();
			} 
		} catch (InputMismatchException e) {
			output.println("That isn't a number");
		} catch (EmptyStackException e) {
			output.println("Error, that number is <= 0");
		}
		Cycle cyc1 = new Cycle();
		Cycle cyc2 = new Cycle(wheels, weight);
		try {
			output.println(cyc1.toString());
			output.println(cyc2.toString());
			output.close();
		} catch (Exception e) {
			
		}
	}
}