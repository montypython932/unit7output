public class Cycle {
	
	private double numOfWheels;
	private double weight;
	
	//default constructor, calls overloaded constructor
	public Cycle() {
		this((double) 100, (double)1000);
	}
	
	//overloaded constructor
	public Cycle(double numOfWheels, double weight) {
		this.numOfWheels = numOfWheels;
		this.weight = weight;
	}

	//returns a Cycle object in a string format
	public String toString() {
		return "Number of Wheels: " + this.numOfWheels + "\nWeight: " + this.weight;
	}
}